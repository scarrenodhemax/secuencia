/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.herencia;

import cl.unab.vina.diseno.objeto.Persona;

/**
 *
 * @author sebac
 */
public class Profesor extends Persona{

    public Profesor(String nombre, int edad) {
        super(nombre, edad);
    }

    @Override
    public void comunicar() {
        System.out.println("Estoy hablando como Profesor");
    }
    
    
    
}

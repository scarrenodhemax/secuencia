/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.herencia;

import cl.unab.vina.diseno.objeto.Persona;

/**
 *
 * @author sebac
 */
public class Alumno extends Persona implements Colorear{
    String carrera;

    public Alumno(String nombre, int edad, String carrera) {
        super(nombre, edad);
        this.carrera = carrera;
    }
    
    public void mostrarMisPropiedades(){
        System.out.println("Mi carrera es: "+this.carrera);
        System.out.println("Mi nombre es: "+this.nombre);
    }

    public String getCarrera() {
        return carrera;
    }

    public void setCarrera(String carrera) {
        this.carrera = carrera;
    }

    @Override
    public void comunicar() {
        System.out.println("Hola estoy hablando como alumno");
    }

    @Override
    public void pintarse() {
        System.out.println("Hola me estoy maquillando");
    }
    
    
    
}

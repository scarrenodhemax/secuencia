/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.main;

import cl.unab.vina.diseno.herencia.Alumno;
import cl.unab.vina.diseno.objeto.Automovil;
import cl.unab.vina.diseno.objeto.Persona;

/**
 *
 * @author sebac
 */
public class Main {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        // TODO code application logic here
        Persona persona;
//        persona = new Persona("Diego", 15);
        //
//        System.out.println("La edad de la persona es: " +persona.getNombre());
        
        
        Alumno alumno = new Alumno("Juan", 25, "Enfermeria");
        
        persona = alumno;
        
        System.out.println("La carrera del alumno es: "+alumno.getCarrera()+" Edad: "+alumno.getEdad());
        
        alumno.mostrarMisPropiedades();
        
        alumno.pintarse();
        
        Automovil automovil = new Automovil();
        
        automovil.pintarse();
        
    }
    
}

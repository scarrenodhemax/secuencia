/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.objeto;

import cl.unab.vina.diseno.herencia.Alumno;
import cl.unab.vina.diseno.herencia.Profesor;
import java.util.List;

/**
 *
 * @author sebac
 */
public class Curso {
    Profesor profesor;
    List<Alumno> listaAlumnos;

    public Profesor getProfesor() {
        return profesor;
    }

    public void setProfesor(Profesor profesor) {
        this.profesor = profesor;
    }

    public List<Alumno> getListaAlumnos() {
        return listaAlumnos;
    }

    public void setListaAlumnos(List<Alumno> listaAlumnos) {
        this.listaAlumnos = listaAlumnos;
    }
    
    
    
}

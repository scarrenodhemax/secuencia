/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package cl.unab.vina.diseno.objeto;

import cl.unab.vina.diseno.dao.AlumnoDAO;
import cl.unab.vina.diseno.dao.CursoDAO;
import cl.unab.vina.diseno.dao.ProfesorDAO;
import cl.unab.vina.diseno.herencia.Alumno;
import cl.unab.vina.diseno.herencia.Profesor;
import java.util.List;

/**
 *
 * @author sebac
 */
public class CreacionCurso {
    private ProfesorDAO profesorDAO = new ProfesorDAO();    
    private AlumnoDAO alumnoDAO = new AlumnoDAO();
    private CursoDAO cursoDAO = new CursoDAO();
    
    private List<Profesor> listaProfesores;
    private List<Alumno> listaAlumnos;
    
    private Curso curso = new Curso();

    public CreacionCurso() {
        this.listaProfesores = profesorDAO.getListaDeProfesores();
        this.listaAlumnos = alumnoDAO.getListaAlumnos();
    }
    
    public void seleccionarProfesor(Profesor profesor){
        this.curso.setProfesor(profesor);
    }
    
    public void seleccionarAlumno(Alumno alumno){
        listaAlumnos.add(alumno);
    }
    
    public void agregarCurso(){
        if(cursoDAO.insertarCurso(this.curso))
            System.out.println("Se ha agregado el curso correctamente");
        else
            System.out.println("No se ha podido agregar el curso");
        
    }
    
}
